FROM node:12.11.1 AS compile-image
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm install
COPY . .
RUN npm run build
EXPOSE 4200

ENTRYPOINT [ "/bin/bash", "-c", "npm run start" ]