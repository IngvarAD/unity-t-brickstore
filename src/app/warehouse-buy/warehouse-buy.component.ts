import { Component } from '@angular/core';
import { WarehouseService } from '../warehouse.service';
import { FormBuilder, Validators } from '@angular/forms';
import { BrickBatch } from '../brick_batch';

@Component({
  selector: 'app-warehouse-buy',
  templateUrl: './warehouse-buy.component.html',
  styleUrls: ['./warehouse-buy.component.css']
})
export class WarehouseBuyComponent {

  constructor(
    private warehouseService: WarehouseService,
    private formBuilder: FormBuilder
  ) { }

  buyBricksForm = this.formBuilder.group({
    quantity: ['', [
      Validators.required,
      Validators.min(0),
      Validators.pattern('[0-9]*')
    ]],
    price: ['',[
      Validators.required,
      Validators.min(0),
      Validators.pattern('[0-9]+([.]?[0-9]{0,2})')
    ]]
  })

  buyBricks(): void {
    let batch = new BrickBatch(
      parseInt(this.buyBricksForm.value.quantity),
      parseFloat(this.buyBricksForm.value.price)
    );
    this.warehouseService.addBricksBatch(batch);
    this.buyBricksForm.reset();
  }
}
