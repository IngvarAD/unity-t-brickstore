import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseBuyComponent } from './warehouse-buy.component';

describe('WarehouseBuyComponent', () => {
  let component: WarehouseBuyComponent;
  let fixture: ComponentFixture<WarehouseBuyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WarehouseBuyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseBuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
