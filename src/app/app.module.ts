import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { WarehouseBuyComponent } from './warehouse-buy/warehouse-buy.component';
import { WarehouseSellComponent } from './warehouse-sell/warehouse-sell.component';
import { WarehouseHistoryComponent } from './warehouse-history/warehouse-history.component';
import { WarehouseComponent } from './warehouse/warehouse.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    WarehouseBuyComponent,
    WarehouseSellComponent,
    WarehouseHistoryComponent,
    WarehouseComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
