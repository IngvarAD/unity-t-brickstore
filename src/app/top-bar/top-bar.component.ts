import { Component } from '@angular/core';
import { AppComponent } from '../app.component'

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent {

  constructor(
    private app: AppComponent
  ) {}

  title = this.app.getTitle();

}
