import { Component, OnInit } from '@angular/core';
import { WarehouseService } from '../warehouse.service';

@Component({
  selector: 'app-warehouse-history',
  templateUrl: './warehouse-history.component.html',
  styleUrls: ['./warehouse-history.component.css']
})
export class WarehouseHistoryComponent implements OnInit {

  constructor(
    private warehouseService: WarehouseService
  ) { }

  history = this.warehouseService.getHistory();

  ngOnInit(): void {
  }

}
