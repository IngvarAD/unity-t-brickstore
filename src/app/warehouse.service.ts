import { Injectable } from '@angular/core';
import { BrickBatch } from './brick_batch';


@Injectable({
  providedIn: 'root'
})
export class WarehouseService {
  state = 0;
  queue:BrickBatch[] = [];
  history:string[] = [];
  order:string[] = [];
  price = 0;

  constructor() { }

  addBricksBatch(bricks_order:BrickBatch) {
    this.state += bricks_order.quantity;
    this.queue.push(bricks_order);
    this.history.push(`Otrzymano ${bricks_order.quantity} cegieł, każda po \
      ${bricks_order.price} | Aktualny stan: ${this.state}`)
  }

  getBricks(quantity:number) {
    this.order = [];
    this.price = 0;

    if (quantity > this.state || this.queue.length == 0) {
      this.notEnoughBricks(quantity);
      return;
    }

    this.getBricksFromNewestBatch(quantity)
    this.state = this.state - quantity;

    this.history.push(`Wydano ${quantity} cegieł za ${this.price} | ${this.order.join(' | ')} | Aktualny stan: ${this.state}`)
    return `Kupiłeś ${quantity} cegieł za ${this.price}`;
  }

  getBricksFromNewestBatch(quantity:number) {
    let batch:BrickBatch = this.queue.shift()!;
    let rest = batch.getBricks(quantity);
    this.order.push(`${quantity-rest} po ${batch.getPrice()}`)

    this.price += (quantity-rest) * batch.getPrice();

    if (rest > 0) {
      this.getBricksFromNewestBatch(rest);
    } else if (batch.getQuantity() != 0) {
      this.queue.unshift(batch);
    }
  }

  notEnoughBricks(quantity:number) {
    this.history.push(`Nie obsłużono zamówienia na ${quantity} \
      cegieł, za mały stan magazynowy (${this.state})`)
      window.alert("Za mało cegieł w magazynie!")
  }

  getHistory() {
    return this.history;
  }
}
