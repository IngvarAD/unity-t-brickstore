import { Component, OnInit } from '@angular/core';
import { WarehouseService } from '../warehouse.service';

@Component({
  selector: 'app-warehouse',
  templateUrl: './warehouse.component.html',
  styleUrls: ['./warehouse.component.css']
})
export class WarehouseComponent implements OnInit {

  constructor(
    private warehouse: WarehouseService
  ) { }

  state = this.warehouse.state

  ngOnInit(): void {
  }

}
