export class BrickBatch {
    constructor (
        public quantity: number,
        public price: number
    ) {}

    getBricks(amount:number) {
        if (amount >= this.quantity) {
            let old_quantity = this.quantity;
            this.quantity = 0;
            return amount - old_quantity;
        }
        this.quantity -= amount;
        return 0;
    }

    getQuantity() {
        return this.quantity;
    }

    getPrice() {
        return this.price;
    }
}