import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { WarehouseBuyComponent } from './warehouse-buy/warehouse-buy.component';
import { WarehouseHistoryComponent } from './warehouse-history/warehouse-history.component';
import { WarehouseSellComponent } from './warehouse-sell/warehouse-sell.component';
import { WarehouseComponent } from './warehouse/warehouse.component';

const routes: Routes = [
  { path: "", component: WarehouseComponent},
  { path: "history", component: WarehouseHistoryComponent },
  { path: "buy", component: WarehouseBuyComponent },
  { path: "sell", component: WarehouseSellComponent },
  { path: "404", component: PageNotFoundComponent },
  { path: "**", redirectTo: "/404" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
