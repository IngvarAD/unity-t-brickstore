import { Component } from '@angular/core';
import { WarehouseService } from '../warehouse.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-warehouse-sell',
  templateUrl: './warehouse-sell.component.html',
  styleUrls: ['./warehouse-sell.component.css']
})
export class WarehouseSellComponent {

  constructor(
    private warehouseService: WarehouseService,
    private formBuilder: FormBuilder
    ) { }

    sellBricksForm = this.formBuilder.group({
      quantity: ['', [
        Validators.required,
        Validators.min(0),
        Validators.pattern('[0-9]*')
      ]],
    })

    sellBricks(): void {
      let quantity = this.sellBricksForm.value.quantity;
      let msg = this.warehouseService.getBricks(parseInt(quantity));
      this.sellBricksForm.reset();
      if (msg) {
        window.alert(msg);
      }
    }
}
